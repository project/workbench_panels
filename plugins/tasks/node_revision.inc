<?php

/**
 * @file
 * Handle the 'node revision view' override task.
 *
 * This plugin overrides node/%node/revisions/%/view and reroutes it to the page manager, where
 * a list of tasks can be used to service this request based upon criteria
 * supplied by access plugins.
 */

/**
 * Specialized implementation of hook_page_manager_task_tasks().
 */
function workbench_panels_node_revision_page_manager_tasks() {
  return array(
    // This is a 'page' task and will fall under the page admin UI
    'task type' => 'page',

    'title' => t('Node template revision'),

    'admin title' => t('Node template revision'),
    'admin description' => t('When enabled, this overrides the default Drupal behavior for displaying nodes at <em>node/%node/revisions/%/view/<em>.'),
    'admin path' => 'node/%node/revisions/%/view',

    // Menu hooks so that we can alter the node/%node menu entry to point to us.
    'hook menu' => 'workbench_panels_node_revision_menu',
    'hook menu alter' => 'workbench_panels_node_revision_menu_alter',

    // This is task uses 'context' handlers and must implement these to give the
    // handler data it needs.
    'handler type' => 'context',
    'get arguments' => 'workbench_panels_node_revision_get_arguments',
    'get context placeholders' => 'workbench_panels_node_revision_get_contexts',

    // Allow this to be enabled or disabled:
    'disabled' => variable_get('workbench_panels_node_revision_disabled', TRUE),
    'enable callback' => 'workbench_panels_node_revision_enable',
  );
}

/**
 * Callback defined by workbench_panels_node_revision_page_manager_tasks().
 *
 * Alter the node view input so that node view comes to us rather than the
 * normal node view process.
 */
function workbench_panels_node_revision_menu_alter(&$items, $task) {
  if (variable_get('workbench_panels_node_revision_disabled', TRUE)) {
    return;
  }

  // Override the node view handler for our purpose.
  $callback = $items['node/%node/revisions/%/view']['page callback'];
  if ($callback == 'node_show' || variable_get('page_manager_override_anyway', FALSE)) {
    $items['node/%node/revisions/%/view']['page callback'] = 'workbench_panels_node_revision_page';
    $items['node/%node/revisions/%/view']['file path'] = $task['path'];
    $items['node/%node/revisions/%/view']['file'] = $task['file'];
  }
  else {
    // automatically disable this task if it cannot be enabled.
    variable_set('workbench_panels_node_revision_disabled', TRUE);
    if (!empty($GLOBALS['workbench_panels_enabling_node_revision'])) {
      drupal_set_message(t('Page manager module is unable to enable node/%node/revisions/%/view because some other module already has overridden with %callback.', array('%callback' => $callback)), 'error');
    }
  }

  // @todo override node revision handler as well?
}

/**
 * Entry point for our overridden node view.
 *
 * This function asks its assigned handlers who, if anyone, would like
 * to run with it. If no one does, it passes through to Drupal core's
 * node view, which is node_page_view().
 */
function workbench_panels_node_revision_page($node) {
  // Load my task plugin
  $task = page_manager_get_task('node_revision');

  // Load the node into a context.
  ctools_include('context');
  ctools_include('context-task-handler');
  $contexts = ctools_context_handler_get_task_contexts($task, '', array($node));

  $output = ctools_context_handler_render($task, '', $contexts, array($node));
  if ($output != FALSE) {
    node_tag_new($node);
    return $output;
  }

  $function = 'node_page_view';
  foreach (module_implements('page_manager_override') as $module) {
    $call = $module . '_page_manager_override';
    if (($rc = $call('node_view')) && function_exists($rc)) {
      $function = $rc;
      break;
    }
  }

  // Otherwise, fall back.
  return $function($current_node);
}

/**
 * Callback to get arguments provided by this task handler.
 *
 * Since this is the node view and there is no UI on the arguments, we
 * create dummy arguments that contain the needed data.
 */
function workbench_panels_node_revision_get_arguments($task, $subtask_id) {
  return array(
    array(
      'keyword' => 'node',
      'identifier' => t('Node being viewed'),
      'id' => 1,
      'name' => 'entity_id:node',
      'settings' => array(),
    ),
  );
}

/**
 * Callback to get context placeholders provided by this handler.
 */
function workbench_panels_node_revision_get_contexts($task, $subtask_id) {
  $test = workbench_panels_node_revision_get_arguments($task, $subtask_id);
  $placeholders = ctools_context_get_placeholders_from_argument($test);
  return $placeholders;
}

/**
 * Callback to enable/disable the page from the UI.
 */
function workbench_panels_node_revision_enable($cache, $status) {
  variable_set('workbench_panels_node_revision_disabled', $status);

  // Set a global flag so that the menu routine knows it needs
  // to set a message if enabling cannot be done.
  if (!$status) {
    $GLOBALS['workbench_panels_enabling_node_revision'] = TRUE;
  }
}
